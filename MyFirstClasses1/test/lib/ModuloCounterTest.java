package lib;

import static org.junit.Assert.*;

import org.junit.Test;

public class ModuloCounterTest {

	@Test
	public void testDefaultconstructor() {
		ModuloCounter mc = new ModuloCounter();
		
		assertEquals("Count field should be initialised to 0.",0 , mc.getcount());
		assertEquals("Max field should be initialised to 10.",10 , mc.getMax());
		assertEquals("cycles field should be initialised to 0.",0 , mc.getCycles());

		}
	
	@Test
	public void testCustomConstructor() {
		ModuloCounter mc = new ModuloCounter(2, 10, 1);
		
		assertEquals("Count field should be initialised to 2.",2 , mc.getcount());
		assertEquals("Max field should be initialised to 10.",10 , mc.getMax());
		assertEquals("cycles field should be initialised to 1.",1 , mc.getCycles());

	}

	@Test
	public void testSetandGetCount() {
		ModuloCounter mc = new ModuloCounter();
		mc.setCount(3);
		assertEquals("Count field should be set to 3.", 3, mc.getcount());
		
	}

	@Test
	public void testSetandGetCount2() {
		ModuloCounter mc = new ModuloCounter();
		ModuloCounter mc2 = new ModuloCounter();
		mc.setCount(-3);
		mc2.setCount(14);
		
		assertEquals("Count field should be set to 0.", 0, mc.getcount());
		assertEquals("Count field should be set to 0.", 0, mc2.getcount());
	}
	@Test
	public void testGetMax() {
		ModuloCounter mc = new ModuloCounter();
		assertEquals("Max field should return 10.", 10, mc.getMax());
		
	}
	
	@Test
	public void testGetCycle() {
		ModuloCounter mc = new ModuloCounter(0,10,3);
		assertEquals("cycles field should return 3.", 3, mc.getCycles());
		
	}

	@Test
	public void testIncrement() {
		ModuloCounter mc = new ModuloCounter();
		mc.increment();
		mc.increment();
		assertEquals("count field should return the value of 2", 2 , mc.getcount()); 
	}

	@Test
	public void testDecrement() {
		ModuloCounter mc = new ModuloCounter(6,10,0);
		mc.decrement();
		mc.decrement();
		assertEquals("count field should return the value of 4", 4 , mc.getcount()); 
	}

	@Test
	public void testIncrementBy() {
		ModuloCounter mc = new ModuloCounter();
		mc.incrementBy(3);
		
		assertEquals("count field should return the value of 3", 3 , mc.getcount()); 
	}

	@Test
	public void testGetCycles() {
		ModuloCounter mc = new ModuloCounter(2, 10, 4);
		
		assertEquals("Cycles field should return the value of 4", 4 , mc.getCycles()); 
	}

}
