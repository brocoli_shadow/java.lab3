package lib;

/**
 * Class that contains name, artist and duration of songs on a cd.
 * @author leice
 *
 */

public class CDTrack {
	
	//Fields
	private String title;
	private String artist;
	private int duration;
	
	//Constructors
	public CDTrack(){
		title = "";
		artist = "";
		duration = 0;
	}
	
	public CDTrack(String title, String artist, int duration){
		this.title = title;
		this.artist = artist;
		if (duration < 0) {
			duration = 0;
		} else {
			this.duration = duration;
		}
	}
	
	//Set methods
	public void setTitle(String title) {
		this.title = title;
	}
	
	public void setArtist(String artist) {
		this.artist = artist;
	}
	
	public void setDuration(int duration) {
		if (duration >= 0) {
			this.duration = duration;
		}
		
	}
	
	//Get methods
	public String getTitle() {
		return title;
	}
	
	public String getArtist() {
		return artist;
	}
	
	public int getDuration() {
		return duration;
	}
	
	@Override
	public String toString() {
		return "CDTrack:[title=" + title + ", artist=" + artist + ", duration=" + duration + "]";
	}
	
	/*Reset method
	public void resetDuration() {
		duration = 0;
		
	why this didnt work when called through the getDuration method with the if because duration is of primitive type
	}*/
	

}
