package lib;
/*ModuloCounter A counter that has a limit over which it wraps around
 * example if max is 5, the counter will increase as follow 0 1 2 3 4 0 1 2 3 ...
 */

public class ModuloCounter {
	
	//Instance variables
	private int count;
	private int max;
	private int cycles;
	
	//Default constructor
	public ModuloCounter() {
		count=0;
		max= 10;
		cycles = 0;
	}
	
	//Custom constructor
	public ModuloCounter(int count, int max, int cycles){
		this.count=count;
		this.max =max;
		this.cycles = cycles;
	}
	
	//The set method when count is set over max- 1 it returns to 0
	public void setCount(int count){
	
	
	if (count < 0 || count > (max-1)){
	count = 0;
	}
	
	else {
		this.count=count;}
	}
	
	//The get method
	public int getcount(){
		return count;
	}
	
	public int getCycles(){
		return cycles;
	}
	public int getMax(){
		return max;
	}
	
	//Increment method where count can only be increment until max-1 then returns to 0
	public void increment(){
		if(count >= 0 && count <= (max-2)){
			count = count + 1;
			
			if (count == max-1){
				cycles++;
			}
		}
		
		else{
			count =0;
		}
	}
	
	//Decrement method
	public void decrement(){
		count--;
		
		if (count <0){
			count = 0;
		}
	}
	
	//The increment by method 
	public void incrementBy(int amount){
		
		count = count + amount;
		
		if (count> max-1){
			count = count % amount;
		}
		
	}
	
	
}
