package main;

import lib.CDTrack;

public class CDTrackApp {

	public static void main(String[] args) {
		
		
		//Creating object using the default constructor and assigning variables using methods
		CDTrack track1 = new CDTrack();
		track1.setTitle("Thriller");
		track1.setArtist("Michael Jackson");
		track1.setDuration(523);
		
		//Creating another object using the custom constructor
		CDTrack track2 = new CDTrack("Singing In The Rain", "Sinatra",325);
		
		//Outputting the details of the first object
		System.out.println("Title=" + track1.getTitle());
		System.out.println("Artist=" + track1.getArtist());
		System.out.println("Duration=" +track1.getDuration());
		
		//Outputting the details of the second object
		System.out.println(track2.toString());
		
		
	}

}
