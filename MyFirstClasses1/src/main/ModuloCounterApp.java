package main;

import lib.ModuloCounter;

public class ModuloCounterApp {

	public static void main(String[] args) {
		
		
		ModuloCounter counter = new ModuloCounter();
		
		
		//Testing wrapping functionality
		for(int i=0 ; i< 50; i++){
			
			counter.increment();
			System.out.print(counter.getcount() + "  ");
			
		}
		
		//Testing cycles functionality
				System.out.println("\n" + counter.getCycles());
		
	}

}
